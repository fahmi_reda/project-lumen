<?php

namespace App\Console\Commands;

use App\Jobs\TestJob;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Queue;

class FireEvent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fire';


    public function handle()
    {
        $user = ["12", "13"];
        // TestJob::dispatch($user);
        // dispatch(new TestJob($user));
        Queue::push(new TestJob(3));
    }
}
